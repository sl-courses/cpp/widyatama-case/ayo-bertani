#include <iostream>
using namespace std;
int main()
{
	//Input 
	//Baris pertama diisi oleh panjang lahan tersebut (1≤P≤10)
	cout << "Masukkan panjang lahan : " ;
	int P;
	cin >> P; 
	//Baris kedua diisi oleh lebar lahan tersebut (1≤L≤25) 
	cout << "Masukkan lebar lahan : " ;
	int L;
	cin >> L;
	//Input data isi lahan Masing2 
	//1 – jika lahan tersebut telah ditanami 
	//0 - jika lahan tersebut kosong
	int M[P][L];
	for (int i=0; i<P; i++)
	{
		for (int j=0; j<L; j++)
		{
			cout << "Masukkan data isi lahan ke (" << i+1 << "," << j+1 << ") : ";
			cin >> M[i][j];
		}
	} 
	//Output 
	//Baris pertama jumlah lahan yang kosong 
	//Baris kedua jumlah lahan yang telah ditanami 
	int jumlahkosong=0, jumlahditanami=0;
	for (int i=0; i<P; i++)
	{
		for (int j=0; j<L; j++)
		{
			if (M[i][j]==0)
			{
				jumlahkosong++;
			}
			else if (M[i][j]==1)
			{
				jumlahditanami++;
			}
		}
	}
	cout << "Jumlah lahan yang kosong : " << jumlahkosong << endl;
	cout << "Jumlah lahan yang telah ditanami : " << jumlahditanami << endl;
	return 0;
}
